# Talk² Bootloader Self-Programming #
The Talk² Bootloader, apart from accepting programming over UART (serial port) as the standard Arduino bootloader does, it's also capable of self-programming the MCU using data stored on the external SPI Flash memory. This feature extends the programming capabilities and allows, for example: firmware upgrades over-the-air, factory reset and even firmware backup and restore.

It's important to note that the Talk² Bootloader is not responsible to "download" and store data into the SPI Flash memory. The bootloader is only responsible for reading data previously stored there. The download operation is normally handled by the main program. The reason for this approach is directly related to the limited bootloader size, as well the intention to reduce complexity and consecutively the number of possible bugs.

##Table of Contents##

[TOC]

## Main Features ##

1. Integrity check using Signature and CRC to prevent corrupted data to be programmed into the MCU;
1. Flexible configuration, capable of multiple firmwares and Application data to live together in the same SPI Flash;
1. Factory reset, normally triggered by an user button during the MCU powering up;
1. Operation feedback by saving results into the MCU's EEPROM for later access.

## Programming Flow ###
The Talk² Bootloader is based on the Optiboot, so it will work as a normal UART programming interface. Additionally the bootloader also reads a reserved EEPROM location, looking for special commands to initiate a self-programming operation:

1. MCU Reset or Startup caused by Watchdog or Power up/Failure (BOD).
1. Bootloader reads the "Bootloader Configuration" stored on the MCU's internal EEPROM.
  1. It verifies the initial 4 bytes to see if they match the "Talk² Bootloader Signature".
  1. Next it verifies the if the "Bootloader Configuration Return Code" bytes are all 'blanked': 0xFF 0xFF 0xFF 0xFF.
    * In case of error on any of the operation above the bootloader exits to the normal operation, interrupting the self-programming and continuing to the UART programming, starting the Application otherwise.
  1. Everything being OK, the bootloader calculates the CheckSum for all "Bootloader Configuration".
    * In case of error, the bootloader writes an error to the "Bootloader Configuration Return Code" area and restart the MCU. As the "Return Code" area is not empty any more, the bootloader will not get to this point again until a new "Bootloader Configuration" is programmed into the MCU's EEPROM.
  1. The next step the bootloader reads the command field from the "Bootloader Configuration", as well the following 8 bytes, which are normally used as parameter(s). For example, the command will tell the bootloader to read the SPI Flash on the specified address and proceed with the self-programming operation.
1. Moving forward the bootloader will read the "Program Pack" from the SPI Flash and perform a few operations to verify if everything is OK before start programming the MCU.
  1. Similar to the previous step, the bootloader looks for the "Talk² Bootloader Signature".
  1. Next it looks for the "Program CheckSum Starting Address", "CheckSum Chunk Size" and "Program Data Starting Address".
  1. The bootloader verifies everything, including if the "Program Data" checksum matches the expected result. This operation happen by chunks, instead of a single CRC result for the whole "Program Data". This approach adds a few extra bytes to the "Program Pack" but reduces the change of a false positive CRC result.
  * Again, if any of the steps above fails, the bootloader will write an error to the "Bootloader Configuration Return Code" area and restart the MCU.
1. At this point everything has been verified and the MCU will be programmed.
  1. The programming happens per page and, from time-to-time, the bootloader resets the MCU's watchdog.
  1. At the end a success code is written to the "Bootloader Configuration Return Code" area and the MCU is restarted.
  * Similar to the situation above, now the "Bootloader Configuration Return Code" on the EEPROM is not blank, skipping the self-programming and starting the Application.

#### Factory Reset Flow ####
The Talk² Bootloader offers a "Factory Reset" functionality, which basically:

1. Writes to the "Bootloader Configuration" a pre-defined/hard-coded configuration in the MCU's EEPROM.
1. After that, the bootloader restart the MCU and let the original programming flow take care - see "Programing Flow" topic.

Note that it depends on the "Factory Program Pack", or any other user "Program Pack" be stored on the SPI Flash correct address. More details can be found on the "Bootloader Configuration" topic. Additionally, enabling write-protection on the "Factory Program Pack" area is a good idea to prevent erasing the data from SPI Flash by mistake.

##### Talk² Whisper Node #####
To trigger the "Factory Reset Flow" on the Talk² Whisper Node board, you must hold the BT1 (Button 1) and power the board. Wait until the LD2 (Yellow Led) start to blink quickly and release the button while the LD2 still blinking. The LD1 (Blue Led) will blink quickly a few times indicating the process was executed.
  
## Data Structures ##
The self-programming operation has some very strict rules to minimise the risk of errors. For everything to work nicely, the data needs to be in the right place and following the correct format. Any exception will prevent the self-programming to happening. Find next the format for all data structure used by the self-programming process, as well the pre-defined addresses, sizes and basic algorithm logic for the CRC calculation.

The examples bellow will be using data represented as HEX values unless specified. Please read "20" as "0x20" (which would be 32 integer). 

### Bootloader Configuration ###
As described on the "Programming Flow", the first step is to look for the "Bootloader Configuration", which is a fixed length of data stored in a pre-defined address of the MCU's Internal EEPROM. The EEPROM has been chosen as it offers the ability to update a single byte, where the external SPI Flash can only handle blocks of at least 4K, making it very inefficient to perform frequent updates on a such small portion of data.

**MCU's EEPROM Bootloader Configuration Address:** 0x03E0 to 0x03FF (32 Bytes)

**SPI Flash Factory Program Pack Address:** 0x00070000 (user for Factory reset)

Bellow an example how a valid "Bootloader Configuration" looks like:

```
|0         3|4   |5   |6         9|10       13|14  |15       18|19    31|
┌───────────┬────┬────┬───────────┬───────────┬────┬───────────┬────────┐
│54|6c|6b|b2│ 01 │ 0a │00|07|00|00│00|00|00|00│ 11 │ff|ff|ff|ff│reserved│
│ signature │ver │cmd │parameter1 │parameter2 │crc │  cmdOut   │13 bytes│
└───────────┴────┴────┴───────────┴───────────┴────┴───────────┴────────┘
```
*The example above is actually the "Factory Reset" "Bootloader Configuration" present on the standard Talk² Bootload source code.*

Next you'll find the detailed description of each field:

#### Signature ####
As the name says it's just a sequence of 4 bytes to help confirming that the MCU's EEPROM data being read is actually a valid "Bootloader Configuration" set.

The signature is:

| Format     | Value     |
|------------|-----------|
|**ASCII:**  |Tlk²       |
|**HEX:**    |54 6c 6b b2|
|**Decimal:**|1416391602 |

#### Version ####
The version must be compatible with the Talk² Bootloader. This will allow to create backward compatibility in future releases as well preventing error of new configuration being used by older bootloader versions.

#### Command ####
The Talk² Bootloader can support multiple operations, where the main one is the self-programming. Saying that the design allow future functionalities to be added like "Backup", "Verify", "Restore", etc.

|Command|Operation                                                                                     |
|-------|----------------------------------------------------------------------------------------------|
|0x0a   | Program the MCU by reading the "Program Pack" on the SPI Flash address from the "Parameter 1"|

#### Parameters 1 and 2 ####
Those are 8 bytes, normally used as 2 unit32_t values, which can be used as a parameters for the "Command". On this example, the "Parameter 1" indicates which address the bootloader should look for a "Program Pack" on the external SPI Flash when running the command "0x0a". "Parameter 2" in this case is not used.

#### CRC ####
This is the result from the calculated CheckSum from the bytes 0 to 13 of the "Bootloader Configuration". The CheckSum is calculated using a simple CRC algorithm. Basically the SUM of a number of bytes followed by the operation "NOT" and adding 1, for example:

Data (from the example):

*54 6c 6b b2 01 0a 00 07 00 00 00 00 00 00*

Calculation (on uint8_t):

*54 + 6c + 6b + b2 + 01 + 0a + 00 + 07 + 00 + 00 + 00 + 00 + 00 + 00 = ef*

*NOT 0xef = 0x10*

*0x10 + 1 = 0x11*

#### Reserved ####
As the name said, those bytes are currently not in use, but they should be left alone for the moment.

### Program Pack ###
The Program Pack is an archive containing not only the "Program Data"/New Firmware, it also includes some important data like the "Program Pack Header", "Program CheckSum" and "Program Data" - the last one being the raw firmware itself. This extra data is required to guide the bootloader through the self-programming process.

#### Program Pack Header ####
Similar to the "Bootloader Configuration", the "Program Pack Header" will start with a signature. It also contains the "CheckSum Chunk Size" (ckSumChunk), which tells how the "Program Data" CheckSum has been calculated. Additionally it informs the SPI Flash address where the "Program CheckSum" has been stored (ckSumAddr) as well its size (ckSumSize). Having a "CheckSum Chunk Size" of 32 (0x20) offers a good balance between size and reliability.

```
|0         3|4         |5         8|9        12|13       16|17       20|
┌───────────┬──────────┬───────────┬───────────┬───────────┬───────────┐
│54|6c|6b|b2│    20    │00|07|00|15│00|00|02|3e│00|07|02|53│00|00|47|ac│
│ signature │ckSumChunk│ ckSumAddr │ ckSumSize │  dataAddr │ dataSize  │
└───────────┴──────────┴───────────┴───────────┴───────────┴───────────┘
```

The "Program Data Address" tells where in the SPI Flash the first byte of the "Program Data" is located. This is a 32 bit address followed by the "Program Data Size" (dataSize), which informs the total program size - that should fit in the MCU's Flash memory.

Although it's not necessary by design, the "CheckSum Address" should preferentially be immediately after the last "Program Pack Header" byte. The same way the "Program Data Address" (dataAddr) should preferentially be immediately after the last "Program CheckSum" byte (ckSumAddr + ckSumSize). This will make sure the "Program Pack" is not fragmented, making it easier to manage.

#### Program CheckSum ####
The "Program CheckSum" is a portion of data containing the result of multiple CheckSum operations over the "Program Data", each operation performing the CheckSum of "CheckSum Chunk Size" bytes, for example, 32 bytes.

```
┌─────────────────┬──────┐
│SPI Flash Address│CkSum │
│-----------------│----- │
│0x00070015       │0x10  │ <- CheckSum result for the "Program Data" from "Program Data Address" to "Program Data Address + CheckSum Chunk Size".
│0x00070016       │0xab  │
│0x00070017       │0x23  │
│...              │...   │
│0x00070253       │0x55  │
└─────────────────┴──────┘
```

Depending on the "Program Data Size", the last CheckSum might be for a smaller number of bytes if the "Program Data Size" is not a multiple of "CheckSum Chunk Size".

#### Program Data ####
The "Program Data" is the firmware data itself. Note that this is the pure binary, a clone of what's supposed to be programmed into the MCU's Flash Memory. It's stored one byte per address:

```
┌─────────────────┬─────┐
│SPI Flash Address│Data │
│-----------------│-----│
│0x00070253       │0x12 │
│0x00070254       │0xfc │
│0x00070255       │0xa4 │
│...              │...  │
│0x000749ff       │0x1c │
└─────────────────┴─────┘
```