/*
 * Talk�Boot for Talk� Whisper Node AVR
 * http://talk2.wisen.com.au
 *
 * This file is part of Talk�Boot.

 * Talk�Boot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Talk�Boot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* Talk� Boot Config */
#define TALK2_BOOT_SIGNATURE                            0x546C6BB2  // Tlk�#define TALK2_MAX_PROG_SIZE                             30720 // Maximum size of a program, basically 32k minus 2k from the bootloader
#define TALK2_FACTORY_CONFIG                            { 0x54, 0x6C, 0x6B, 0xB2, 0x01, 0x0A, 0x00, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x11, 0xFF, 0xFF, 0xFF, 0xFF }#define TALK2_EEPROM_BOOT_ADDRESS                       0x03E0  // Last 32 bytes#define TALK2_EEPROM_BOOT_SIZE                          19  // The Bootload config buffer size. That's 32 but we use only 19 for now.#define TALK2_EEPROM_BOOT_VERSION                       0x01  // The Bootload config format version#define TALK2_EEPROM_BOOT_SIGNATURE_POSITION            0   // Position for the Signature, which is an unit32_t#define TALK2_EEPROM_BOOT_VER_POSITION                  4   // Position for the Version byte#define TALK2_EEPROM_BOOT_CMD_POSITION                  5   // Position for the CMD byte#define TALK2_EEPROM_BOOT_PARM1_POSITION                6   // Position for the Parameter 1, which is an unit32_t#define TALK2_EEPROM_BOOT_PARM2_POSITION                10  // Position for the Parameter 2, which is an unit32_t#define TALK2_EEPROM_BOOT_CKSUM_POSITION                14  // Position for the ckSum byte#define TALK2_EEPROM_BOOT_OUTPUT_POSITION               15  // The start position for the OUTPUT#define TALK2_EEPROM_BOOT_CKSUM_SIZE                    14  // The amount of byte we calculate ckSum
#define TALK2_SPI_FLASH_DEVICE_ID                       0xEF30  // Define SPI Flash Device ID - Based on the Chip manufacturer ID#define TALK2_SPI_PROG_HEADER_SIZE                      21  // The Size of the Program Package Header#define TALK2_SPI_PROG_HEADER_PROG_SIGNATURE_POSITION   0   // Position for the Signature, which is an unit32_t#define TALK2_SPI_PROG_HEADER_CKSUM_CHUNK_SIZE_POSITION 4   // Position where to find the Check Sum Chunk size
#define TALK2_SPI_PROG_HEADER_CKSUM_ADDR_POSITION       5   // Position for the Check Sum Address, which is an unit32_t
#define TALK2_SPI_PROG_HEADER_CKSUM_SIZE_POSITION       9   // Position for the Check Sum Size, which is an unit32_t
#define TALK2_SPI_PROG_HEADER_PROG_ADDR_POSITION        13  // Position for the Program Address, which is an unit32_t#define TALK2_SPI_PROG_HEADER_PROG_SIZE_POSITION        17  // Position for the Program Size, which is an unit32_t#define TALK2_SPI_MAX_PROG_CHUNK                        256 // The Maximum program chunk size, which is used to calculated the Check Sum

// Return Messages/Output
#define TALK2_BOOT_MSG__INVALID_EEPROM_SIGNATURE 	      0x00FF0001  // The signature does not match - Do not update EEPROM as it might be user's data#define TALK2_BOOT_MSG__INVALID_EEPROM_VERSION  	      0x00FF0002  // The version is not supported#define TALK2_BOOT_MSG__INVALID_EEPROM_CKSUM    	      0x00FF0003  // The ckSum does not match#define TALK2_BOOT_MSG__OK_MCU_PROGRAMMED        	      0x0A000001  // MCU has been programmed#define TALK2_BOOT_MSG__INVALID_SPI_DEVICE_ID			      0x0AFF0001  // The SPI Flash Device ID does not match what we expect#define TALK2_BOOT_MSG__INVALID_PROG_SIGNATURE 		      0x0AFF0002  // The Program Header signature does not match#define TALK2_BOOT_MSG__INVALID_PROG_SIZE			          0x0AFF0003 // The Program size is too big or not multiple of 2
#define TALK2_BOOT_MSG__INVALID_PROG_CKSUM              0x0AFF0004 // The Program Check Sum is not matching the store check sum

/* Buttons - Define Pins and Ports */
#define TALK2_BTN_PIN       PIND
#define TALK2_BTN_1         PIND4
#define TALK2_BTN_2         PIND5

#define TALK2_LED1_DDR	    DDRD
#define TALK2_LED1_PIN	    PIND
#define TALK2_LED1		      PIND6
#define TALK2_LED2_DDR	    DDRB
#define TALK2_LED2_PIN	    PINB
#define TALK2_LED2		      PINB1

/* SPI - Some copied from Arduino SPI Library */
#define SPI_DDR             DDRB
#define SPI_PORT            PORTB
#define SPI_MOSI_PIN        PINB3
#define SPI_MISO_PIN        PINB4
#define SPI_SCK_PIN         PINB5

// SPI Select and Unselect Chip Macros
#define HAL_SPI_SELECT(PORT,PIN)   { PORT &= ~(_BV(PIN)); }
#define HAL_SPI_UNSELECT(PORT,PIN) { PORT |= _BV(PIN); }

#ifndef LSBFIRST
#define LSBFIRST 0
#endif
#ifndef MSBFIRST
#define MSBFIRST 1
#endif

#define SPI_CLOCK_DIV4    0x00
#define SPI_CLOCK_DIV16   0x01
#define SPI_CLOCK_DIV64   0x02
#define SPI_CLOCK_DIV128  0x03
#define SPI_CLOCK_DIV2    0x04
#define SPI_CLOCK_DIV8    0x05
#define SPI_CLOCK_DIV32   0x06

#define SPI_MODE0         0x00
#define SPI_MODE1         0x04
#define SPI_MODE2         0x08
#define SPI_MODE3         0x0C

#define SPI_MODE_MASK     0x0C  // CPOL = bit 3, CPHA = bit 2 on SPCR#define SPI_CLOCK_MASK    0x03  // SPR1 = bit 1, SPR0 = bit 0 on SPCR#define SPI_2XCLOCK_MASK  0x01  // SPI2X = bit 0 on SPSR/*
 * SPI Flash Registers
 * They should be compatible with W25X40 Winbond Memory
 */
#define SPIFLASH_REG__WRITE_ENABLE    0x06
#define SPIFLASH_REG__WRITE_DISABLE   0x04

#define SPIFLASH_REG__WRITE_STATUS    0x01
#define SPIFLASH_REG__READ_STATUS     0x05
#define SPIFLASH_REG__READ_DATA       0x03
#define SPIFLASH_REG__WRITE_DATA      0x02
#define SPIFLASH_REG__JEDEC_ID        0x9F
#define SPIFLASH_REG__UNIQUE_ID       0x4B

#define SPIFLASH_REG__ERASE_4K        0x20
#define SPIFLASH_REG__ERASE_CHIP      0x60

#define SPIFLASH_REG__POWER_DOWN      0xB9
#define SPIFLASH_REG__POWER_UP        0xAB
